import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

public class CashBox extends Thread {
	public static int numberOfKithenSets = 1000;
	private static Object mutex = new Object();
	
	static AtomicInteger atomicInteger = new AtomicInteger();
	
	static ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	static ReadLock readLock;
	static WriteLock writeLock;
	
	static {
		readLock = lock.readLock();
		writeLock = lock.writeLock();
	}
	
	@Override
	public void run() {
		while (true) {
			if (new Random().nextInt(10) < 1)
				read();
			else
				sell(new Random().nextInt(10) + 1);
			if (numberOfKithenSets <= 0)
				break;
		}
		
	}

	private void sell(int nKitchenSets) {
//		synchronized (mutex) {
		writeLock.lock();
		if (nKitchenSets < numberOfKithenSets) {
			try {
				sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			numberOfKithenSets -= nKitchenSets;
		}
		else if (numberOfKithenSets > 0) {
			numberOfKithenSets = 0;
		}
		readLock.lock();
		writeLock.unlock();
		System.out.println("Write: " + numberOfKithenSets);
		readLock.unlock();
//		}
	}

	private void read() {
//		synchronized (mutex) {
//			System.out.println(numberOfKithenSets);
//		}
		readLock.lock();
		System.out.println(numberOfKithenSets);
		readLock.unlock();
	}
}