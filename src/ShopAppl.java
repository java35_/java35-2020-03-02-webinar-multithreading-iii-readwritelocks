public class ShopAppl {
	
	static int N_CASH_BOXES = 2;
	
	public static void main(String[] args) throws InterruptedException {
		CashBox[] cashboxes = new CashBox[N_CASH_BOXES];
//		CashBox cashBox1 = new CashBox();
//		CashBox cashBox2 = new CashBox();
//		CashBox cashBox3 = new CashBox();
//		CashBox cashBox4 = new CashBox();
		
		for (int i = 0; i < N_CASH_BOXES; i++) {
			cashboxes[i] = new CashBox();
			cashboxes[i].start();
		}
		
		for (CashBox cashBox : cashboxes) {
			cashBox.join();
		}
		
//		cashBox1.start();
//		cashBox2.start();
//		cashBox3.start();
//		cashBox4.start();
//		
//		cashBox1.join();
//		cashBox2.join();
//		cashBox3.join();
//		cashBox4.join();
		
		System.out.println("Stock balance: " + CashBox.numberOfKithenSets);
	}
}